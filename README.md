# Legacy Social Network App

This project is intended to be a PoC of an application displaying data from a legacy social network. It's still a work in progress and there is room for improvement in what has already been made.

## Installation

Clone the repository and start the project with docker compose

```bash
docker-compose up
```

## Usage
Three containers are run with this project, one with an instance of a Neo4J database, another one with an api that interfaces with the database, and a third helper application that populates the database with the initial entities and relationships provided in the sample files.

Configurations for the project can be supplied at runtime through an .env file for Docker Compose. A sample .env file has been checked into source control for reference.

```bash
NEO4J_HOST=
NEO4J_BOLT_PORT=
NEO4J_USER=
NEO4J_PASSWORD=
```


Neo4j browser can be found at the standard port:

- http://localhost:7474/browse

The webapp is exposing an api at:

- http://localhost:8080

There is currently no frontend. 

Further instructions on how to invoke the api will be added later.


## Next Steps
Besides proceeding with the rest of the required functionality, the following topics should be addressed for the existing code:

- The python webapp listens on port 5000 in the container despite the configuration provided through compose. This is an issue with Flask.
- Refactoring on the controllers
- Inclusion of tests
- Addition of CI jobs to run the tests (including security checks)
- Improvement of the method for loading the initial data (original files should be parsed to extract entities and relationships, which for now was done manually)

