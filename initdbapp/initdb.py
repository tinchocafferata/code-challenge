from neomodel import StructuredNode, StringProperty, ArrayProperty, RelationshipTo, Relationship, config

config.DATABASE_URL = 'bolt://user:pass@social-database:7687'

class Person(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)
    phone = ArrayProperty(unique_index=True)
    friend = Relationship('Person', 'FRIENDS_WITH')
    classmate = Relationship('Person', 'STUDIES_WITH')
    school = RelationshipTo('School', 'ATTENDS')
    team = RelationshipTo('Team', 'PLAYS_FOR')
    car = RelationshipTo('Car', 'DRIVES')

class School(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)
    district = RelationshipTo('District', 'BELONGS_TO')

class District(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)

class Team(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)

class Car(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    make = StringProperty(unique_index=True, required=True)
    model = StringProperty(unique_index=True, required=True)


def load_database():
    #Dummy hardcoded instructions to load the initial data provided in the text files
    #Store entities
    ally =  Person(src_db_id='A', name='Ally').save()
    molly =  Person(src_db_id='B', name='Molly').save()
    emilio =  Person(src_db_id='C', name='Emilio').save()
    kieff =  Person(src_db_id='G', name='Kieff').save()
    martin =  Person(src_db_id='H', name='Martin').save()
    anthony =  Person(src_db_id='Z', name='Anthony', phone=[1234,2345]).save()
    brad =  Person(src_db_id='I', name='Brad').save()
    joe =  Person(src_db_id='J', name='Joe').save()
    judd =  Person(src_db_id='D', name='Judd').save()
    demm =  Person(src_db_id='L', name='Demm').save()
    unknown =  Person(src_db_id='K', name='unknown').save()
    shermer_s =  School(src_db_id='X', name='Shermer').save()
    shermer_d =  District(src_db_id='Y', name='Shermer').save()
    bulldogs =  Team(src_db_id='Q', name='Bulldogs').save()
    vw =  Car(src_db_id='M', make='Volkswagen', model='Karmann Ghia').save()

    #Store relationships
    ally.classmate.connect(molly)
    ally.friend.connect(emilio)
    molly.friend.connect(emilio)
    ally.school.connect(shermer_s)
    molly.school.connect(shermer_s)
    shermer_s.district.connect(shermer_d)
    emilio.team.connect(bulldogs)
    emilio.friend.connect(kieff)
    kieff.friend.connect(martin)
    martin.friend.connect(unknown)
    anthony.friend.connect(brad)
    brad.friend.connect(unknown)
    molly.car.connect(vw)
    ally.friend.connect(joe)
    joe.friend.connect(unknown)
    judd.friend.connect(demm)
    demm.friend.connect(unknown)

if __name__ == "__main__":
    load_database()