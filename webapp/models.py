from neomodel import StructuredNode, StringProperty, ArrayProperty, RelationshipTo, Relationship

class Person(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)
    phone = ArrayProperty(unique_index=True)
    friend = Relationship('Person', 'FRIENDS_WITH')
    classmate = Relationship('Person', 'STUDIES_WITH')
    school = RelationshipTo('School', 'ATTENDS')
    team = RelationshipTo('Team', 'PLAYS_FOR')
    car = RelationshipTo('Car', 'DRIVES')

class School(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)
    district = RelationshipTo('District', 'BELONGS_TO')

class District(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)

class Team(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    name = StringProperty(unique_index=True, required=True)

class Car(StructuredNode):
    src_db_id = StringProperty(unique_index=True, required=True)
    make = StringProperty(unique_index=True, required=True)
    model = StringProperty(unique_index=True, required=True)