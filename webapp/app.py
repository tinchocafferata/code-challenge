#!/usr/bin/env python
from neomodel import config
from flask import Flask
from models import *
from controllers import backend_api
import logging
import os


url = os.getenv("NEO4J_URI", "bolt://social-database:7687")
db_hostname = os.getenv("NEO4J_HOST")
db_bolt_port = os.getenv("NEO4J_BOLT_PORT")
db_username = os.getenv("NEO4J_USER")
db_password = os.getenv("NEO4J_PASSWORD")

#port = os.getenv("APP_PORT", 8080)
#config.DATABASE_URL = 'bolt://user:pass@social-database:7687'
config.DATABASE_URL = 'bolt://' + db_username + ':' + db_password + '@' + db_hostname + ':' + db_bolt_port
app = Flask(__name__, static_url_path='/static/')
app.register_blueprint(backend_api)



if __name__ == "__main__":
    logging.info('Running on port 8080')
    app.run()