from flask import Flask, Blueprint, request, jsonify, g, Response
from models import *
import logging
import json

backend_api = Blueprint('backend_api', __name__)

@backend_api.route("/echo", methods=['POST'])
def echo_controller():
    return jsonify(request.json)


@backend_api.route('/person',methods = ['GET', 'POST'])
def people_controller():
   if request.method == 'GET':
      request_payload = request.json

      #TO DO: validate the data in the name field
      #TO DO: Method "first" returns the first matched node. The "get" method was returning many, and it expects only one (find out why)
      p = Person.nodes.first(src_db_id=request_payload['src_db_id'])

      #Construct json object with all the user data to send in the response
      user_data = {}
      user_data['name'] = p.name
      user_data['src_db_id'] = p.src_db_id
      response_payload = json.dumps(user_data)

      return jsonify(response_payload)
   else:
      request_payload = request.json

      #TO DO: validate the input data, handle any exceptions and confirm that the node was created
      #TO DO: include phone numbers in the creation in case this data is provided
      p = Person(src_db_id=request_payload['src_db_id'], name=request_payload['name']).save()
      
      #Debug code to confirm that the node was created
      #p = Person.nodes.get(name=person_data['name'])
      #print(p.name)

      #TO DO: adjust the response message according to the results of the operation
      return jsonify(request.json)

@backend_api.route('/school',methods = ['GET', 'POST'])
def school_controller():
   if request.method == 'GET':
      request_payload = request.json

      #TO DO: validate the data in the name field
      #TO DO: Method "first" returns the first matched node. The "get" method was returning many, and it expects only one (find out why)
      s = School.nodes.first(src_db_id=request_payload['src_db_id'])
      print(s.name)

      #Construct json object with all the user data to send in the response
      school_data = {}
      school_data['name'] = s.name
      school_data['src_db_id'] = s.src_db_id
      response_payload = json.dumps(school_data)

      return jsonify(response_payload)
   else:
      request_payload = request.json

      #TO DO: validate the input data, handle any exceptions and confirm that the node was created
      #TO DO: include phone numbers in the creation in case this data is provided
      s = School(src_db_id=request_payload['src_db_id'], name=request_payload['name']).save()

      #TO DO: adjust the response message according to the results of the operation
      return jsonify(request.json)

@backend_api.route('/district',methods = ['GET', 'POST'])
def district_controller():
   if request.method == 'GET':
      request_payload = request.json

      #TO DO: validate the data in the name field
      #TO DO: Method "first" returns the first matched node. The "get" method was returning many, and it expects only one (find out why)
      d = District.nodes.first(src_db_id=request_payload['src_db_id'])

      #Construct json object with all the user data to send in the response
      district_data = {}
      district_data['name'] = d.name
      district_data['src_db_id'] = d.src_db_id
      response_payload = json.dumps(district_data)

      return jsonify(response_payload)
   else:
      request_payload = request.json

      #TO DO: validate the input data, handle any exceptions and confirm that the node was created
      #TO DO: include phone numbers in the creation in case this data is provided
      d = District(src_db_id=request_payload['src_db_id'], name=request_payload['name']).save()

      #TO DO: adjust the response message according to the results of the operation
      return jsonify(request.json)


@backend_api.route('/team',methods = ['GET', 'POST'])
def team_controller():
   if request.method == 'GET':
      request_payload = request.json

      #TO DO: validate the data in the name field
      #TO DO: Method "first" returns the first matched node. The "get" method was returning many, and it expects only one (find out why)
      t = Team.nodes.first(src_db_id=request_payload['src_db_id'])

      #Construct json object with all the user data to send in the response
      team_data = {}
      team_data['name'] = t.name
      team_data['src_db_id'] = t.src_db_id
      response_payload = json.dumps(team_data)

      return jsonify(response_payload)
   else:
      request_payload = request.json

      #TO DO: validate the input data, handle any exceptions and confirm that the node was created
      #TO DO: include phone numbers in the creation in case this data is provided
      t = Team(src_db_id=request_payload['src_db_id'], name=request_payload['name']).save()

      #TO DO: adjust the response message according to the results of the operation
      return jsonify(request.json)


@backend_api.route('/car',methods = ['GET', 'POST'])
def car_controller():
   if request.method == 'GET':
      request_payload = request.json

      #TO DO: validate the data in the name field
      #TO DO: Method "first" returns the first matched node. The "get" method was returning many, and it expects only one (find out why)
      c = Car.nodes.first(src_db_id=request_payload['src_db_id'])

      #Construct json object with all the user data to send in the response
      car_data = {}
      car_data['make'] = c.make
      car_data['model'] = c.model
      car_data['src_db_id'] = c.src_db_id
      response_payload = json.dumps(car_data)

      return jsonify(response_payload)
   else:
      request_payload = request.json

      #TO DO: validate the input data, handle any exceptions and confirm that the node was created
      #TO DO: include phone numbers in the creation in case this data is provided
      c = Car(src_db_id=request_payload['src_db_id'], make=request_payload['make'], model=request_payload['model']).save()

      #TO DO: adjust the response message according to the results of the operation
      return jsonify(request.json)